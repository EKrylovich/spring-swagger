package com.company.controller;

import com.company.entity.Message;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ekrylovich
 * on 1/17/18.
 */
@RestController
@Api(value="My Rest Controller", description="This is simple rest controller to test swagger integration")
public class MyRestController {
    @RequestMapping("/")
    public String welcome() {
        return "Welcome to simple Spring JAva rest service example.";
    }

    @RequestMapping(value = "/hello/{player}", produces = "application/json")
    @ApiOperation(value = "View a message from Server", response = Message.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved message"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public Message message(@PathVariable String player) {
        return new Message(player, "Hello " + player);
    }
}
